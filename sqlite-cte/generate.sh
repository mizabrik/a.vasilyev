sqlite3 dag.db << EOF
CREATE TABLE DAG (id int, parent int);

INSERT INTO DAG (id, parent) VALUES
  (2, 1),
  (3, 1),
  (4, 1),
  (5, 2),
  (5, 1),
  (6, 3),
  (7, 6),
  (8, 7),
  (9, 8),
  (10, 9);
EOF
