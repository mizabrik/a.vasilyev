#include <error.h>
#include <stdio.h>

#include <sqlite3.h>

int print_callback(void *data, int size, char **row, char **cols) {
  printf("%s\n", row[0]);

  return 0;
}

int main(int argc, char **argv) {
  if (argc != 4) {
    error(-1, 0, "USAGE: %s  DATABASE ID DEPTH", argv[0]);
  }

  sqlite3 *db;
  if (sqlite3_open(argv[1], &db)) {
    error(-1, 0, "Could not open %s", argv[1]);
  }

  char query[1024];
  sprintf(query,
          "WITH RECURSIVE Parents (id, depth) AS ( "
          "  SELECT %s, 0 "
          "  UNION ALL "
          "  SELECT parent, depth + 1 "
          "  FROM Parents INNER JOIN DAG ON Parents.id = DAG.id "
          "  WHERE depth <  %s "
          ") SELECT DISTINCT id FROM Parents WHERE depth > 0",
          argv[2], argv[3]);

  char *error_message;
  if(sqlite3_exec(db, query, print_callback, NULL, &error_message) != SQLITE_OK)
    error(0, 0, "%s", error_message);

  sqlite3_close(db);

  return 0;
}
