INSERT INTO "Material" (id, name, type, price, measure) VALUES
  (1, 'сталь', 'металл', 100, 'кг'),
  (2, 'латунь', 'металл', 50, 'кг'),
  (3, 'чугун', 'металл', 75, 'кг'),
  (4, 'дуб', 'дерево', 50, 'м'),
  (5, 'ясень', 'дерево', 25, 'м'),
  (6, 'кожа', 'отделка', 50, 'м^2'),
  (7, 'искусственная кожа', 'отделка', 30, 'м^2'),
  (8, 'шёлк', 'отделка', 70, 'м^2'),
  (9, 'искуственный шёлк', 'отделка', 25, 'м^2');

INSERT INTO "MaterialAlternatives" (material, alternative) VALUES
  (2, 1),
  (5, 4),
  (6, 7),
  (7, 6),
  (8, 9),
  (9, 8);

INSERT INTO "Product" (id, name, lead_time) VALUES
  (1, 'Продукт 1', interval '10 hours'),
  (2, 'Продукт 2', interval '10 hours'),
  (3, 'Продукт 3', interval '25 hours'),
  (4, 'Продукт 4', interval '25 hours'),
  (5, 'Продукт 5', interval '25 hours'),
  (6, 'Продукт 6', interval '25 hours'),
  (7, 'Продукт 7', interval '25 hours'),
  (8, 'Продукт 8', interval '50 hours'),
  (9, 'Продукт 9', interval '50 hours');

INSERT INTO "ProductMaterials" (product, material, quantity) VALUES
  (1, 1, 5),
  (1, 6, 3),
  (2, 1, 8),
  (2, 2, 2),
  (2, 3, 5),
  (3, 8, 4),
  (4, 7, 1),
  (4, 9, 1),
  (5, 5, 2),
  (6, 6, 6),
  (7, 7, 2),
  (8, 8, 20),
  (9, 8, 10),
  (9, 9, 10);

INSERT INTO "EquipmentType" (id, vendor, name, lifetime) VALUES
  (1, 'Gnusmas', 'X-O-Mator 5000', interval '10 years'),
  (2, 'Gnusmas', 'X-O-Mator 5000HD', interval '5 years'),
  (3, 'Pear', 'jProducer', interval '1 year'),
  (4, 'Simons', 'Cat X10', interval '9 years'),
  (5, 'Byarne', 'C++', interval '50 years');

INSERT INTO "Equipment" (id, type, usage_start) VALUES
  (1, 1, date '2006-06-01'),
  (2, 1, date '2010-01-01'),
  (3, 1, date '2010-01-01'),
  (4, 2, date '2014-04-02'),
  (5, 3, date '2016-02-01'),
  (6, 3, date '2016-02-01'),
  (7, 4, date '2008-02-04'),
  (8, 5, date '1983-03-01');

INSERT INTO "ProductEquipment" (product, equipment) VALUES
  (1, 1),
  (1, 4),
  (2, 1),
  (3, 4),
  (3, 5),
  (4, 1),
  (4, 3),
  (4, 4),
  (5, 3),
  (6, 2),
  (7, 2),
  (8, 3),
  (8, 4),
  (8, 5),
  (9, 1),
  (9, 3);
