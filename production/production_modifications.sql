-- Попробуем удалить шёлк --- сработает ограничение внешнего ключа
-- (неверно при добавлении триггера)
DELETE FROM "Material" WHERE id = 8;

-- Увеличим число используемого искусственного шёлка, если он уже используется
UPDATE "ProductMaterials" PMO
SET quantity = quantity + (
  SELECT quantity FROM "ProductMaterials" PM
  WHERE PM.product = PMO.product AND material = 8
) WHERE 8 IN (SELECT material FROM "ProductMaterials" PM WHERE PM.product = PMO.product)
AND material = 9;

-- Изменим во всех остальных изделиях шёлк на искусственный
UPDATE "ProductMaterials" PMO
SET material = 9
WHERE material = 8
AND 9 NOT IN (SELECT material FROM "ProductMaterials" PM WHERE PM.product = PMO.product);

-- Удалим альтернативы с шёлком
DELETE FROM "MaterialAlternatives"
WHERE material = 8 OR alternative = 8;

-- С чистой совестью удалим шёлк
DELETE FROM "Material" WHERE id = 8;
