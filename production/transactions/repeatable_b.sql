START TRANSACTION ISOLATION LEVEL REPEATABLE READ;

SELECT pg_sleep(2);

UPDATE "Material" SET type = 'металл' WHERE id = 8;

UPDATE "Material" SET price = price + money '10'  WHERE id = 8;

COMMIT;
