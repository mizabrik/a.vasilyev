START TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

UPDATE "Material" SET price = price + money '10' WHERE id = 8;

SELECT pg_sleep(2);

UPDATE "Material" SET type = 'металл' WHERE id = 8;

SELECT pg_sleep(4);

ROLLBACK;
