SELECT * FROM "Material";

START TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

UPDATE "Material" SET price = price + money '10' WHERE id = 8;

SELECT pg_sleep(4);

SELECT * FROM "Material";

SELECT pg_sleep(4);

SELECT * FROM "Material";

COMMIT;
