SELECT * FROM "Material";

START TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT * FROM "Material";
SELECT sum(price) FROM "Material";

SELECT pg_sleep(4);

SELECT * FROM "Material";

SELECT pg_sleep(4);

SELECT sum(price) FROM "Material";
COMMIT;
