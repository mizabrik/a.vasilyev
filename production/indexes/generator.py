#!/usr/bin/env python

import psycopg2
import random
from datetime import timedelta as td

conn = psycopg2.connect("dbname='mizabrik' user='mizabrik'")
cur = conn.cursor()

MATERIALS = 10000

PRODUCTS = 700000

def generate_materials(cur):
    types = ['металл', 'дерево', 'отделочный материал']
    materials = ((i, "Material {}".format(i), random.choice(types), random.randint(50, 500)) for i in range(MATERIALS))
    cur.executemany("""INSERT INTO "Material" (id, name, type, price, measure) VALUES
            (%s, %s, %s, %s, 'measure')""", materials)

def generate_products(cur):
    intervals = [td(days=1), td(hours=10), td(hours=1), td(days=5), td(days=2)]
    for i in range(PRODUCTS):
        product = (i, "Product {}".format(i))
        cur.execute("""INSERT INTO "Product" (id, name, lead_time) VALUES
                (%s, %s, interval '1 day')""", product)

        for material in random.sample(range(MATERIALS), random.randint(2, 4)):
            pm = (i, material, random.randint(1, 10))
            cur.execute("""INSERT INTO "ProductMaterials" (product, material, quantity) VALUES (%s, %s, %s)""",
                     pm)

        if i % 20000 == 0:
            print("{} products".format(i))

if __name__ == '__main__':
    conn = psycopg2.connect("dbname='mizabrik' user='mizabrik'")
    cur = conn.cursor()

    generate_materials(cur)
    generate_products(cur)

    conn.commit()
