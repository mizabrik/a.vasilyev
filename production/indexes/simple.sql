EXPLAIN ANALYZE SELECT * FROM "Product" WHERE lead_time = interval '1 hour';

CREATE INDEX Product_LeadIndex ON "Product" (lead_time);

EXPLAIN ANALYZE SELECT * FROM "Product" WHERE lead_time = interval '1 hour';

DROP INDEX Product_LeadIndex;
