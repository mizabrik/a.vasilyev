EXPLAIN ANALYZE SELECT product, quantity * price
FROM "Material" INNER JOIN "ProductMaterials" ON id = material
WHERE price = money '100' AND quantity < 3;

CREATE INDEX ProductMaterials_Material ON "ProductMaterials" (quantity);

EXPLAIN ANALYZE SELECT product, quantity * price
FROM "Material" INNER JOIN "ProductMaterials" ON id = material
WHERE price = money '100' AND quantity < 3;

CREATE INDEX Material_Price ON "Material" (price);

EXPLAIN ANALYZE SELECT product, quantity * price
FROM "Material" INNER JOIN "ProductMaterials" ON id = material
WHERE price = money '100' AND quantity < 3;

DROP INDEX ProductMaterials_Material;

CREATE INDEX ProductMaterials_Material ON "ProductMaterials" (material, quantity);

EXPLAIN ANALYZE SELECT product, quantity * price
FROM "Material" INNER JOIN "ProductMaterials" ON id = material
WHERE price = money '100' AND quantity < 3;

DROP INDEX ProductMaterials_Material;
DROP INDEX Material_Price;
