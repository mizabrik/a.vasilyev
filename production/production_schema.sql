CREATE TABLE "Product" (
  "id" int NOT NULL,
  "name" varchar(256) NOT NULL,
  "lead_time" interval NOT NULL,
  CONSTRAINT Product_pk PRIMARY KEY ("id")
);



CREATE TABLE "ProductMaterials" (
  "product" int NOT NULL,
  "material" int NOT NULL,
  "quantity" int NOT NULL CONSTRAINT positive_quantity CHECK(quantity > 0),
  CONSTRAINT ProductMaterials_pk PRIMARY KEY ("product", "material")
);



CREATE TABLE "Material" (
  "id" int NOT NULL,
  "name" varchar(256) NOT NULL,
  "type" varchar(256) NOT NULL,
  "price" money NOT NULL CONSTRAINT positive_price CHECK(price > money '0'),
  "measure" varchar(32) NOT NULL,
  CONSTRAINT Material_pk PRIMARY KEY ("id")
);



CREATE TABLE "MaterialAlternatives" (
  "material" int NOT NULL,
  "alternative" int NOT NULL,
  CONSTRAINT MaterialAlternative_Irreflexive CHECK("material" != "alternative"),
  CONSTRAINT MaterialAlternative_pk PRIMARY KEY ("material", "alternative")
);



CREATE TABLE "EquipmentType" (
  "id" int NOT NULL,
  "vendor" varchar(256) NOT NULL,
  "name" varchar(256) NOT NULL,
  "lifetime" interval NOT NULL,
  CONSTRAINT EquipmentType_pk PRIMARY KEY ("id")
);



CREATE TABLE "Equipment" (
  "id" int NOT NULL,
  "type" int NOT NULL,
  "usage_start" DATE NOT NULL,
  CONSTRAINT Equipment_pk PRIMARY KEY ("id")
);



CREATE TABLE "ProductEquipment" (
  "product" int NOT NULL,
  "equipment" int NOT NULL,
  CONSTRAINT ProductEquipment_pk PRIMARY KEY ("product", "equipment")
);




ALTER TABLE "ProductMaterials" ADD CONSTRAINT "ProductMaterials_fk0" FOREIGN KEY ("product") REFERENCES "Product"("id") ON DELETE CASCADE;
ALTER TABLE "ProductMaterials" ADD CONSTRAINT "ProductMaterials_fk1" FOREIGN KEY ("material") REFERENCES "Material"("id");


ALTER TABLE "MaterialAlternatives" ADD CONSTRAINT "MaterialAlternatives_fk0" FOREIGN KEY ("material") REFERENCES "Material"("id") ON DELETE CASCADE;
ALTER TABLE "MaterialAlternatives" ADD CONSTRAINT "MaterialAlternatives_fk1" FOREIGN KEY ("alternative") REFERENCES "Material"("id") ON DELETE CASCADE;


ALTER TABLE "Equipment" ADD CONSTRAINT "Equipment_fk0" FOREIGN KEY ("type") REFERENCES "EquipmentType"("id");

ALTER TABLE "ProductEquipment" ADD CONSTRAINT "ProductEquipment_fk0" FOREIGN KEY ("product") REFERENCES "Product"("id") ON DELETE CASCADE;
ALTER TABLE "ProductEquipment" ADD CONSTRAINT "ProductEquipment_fk1" FOREIGN KEY ("equipment") REFERENCES "EquipmentType"("id");
