-- Определить изделие, с наибольшей длительностью производства
SELECT name FROM "Product"
WHERE lead_time IN (SELECT min(lead_time) FROM "Product");

-- Вывести список оборудования, требуещего замены в ближайшие полгода
SELECT E.id
FROM "Equipment" E INNER JOIN "EquipmentType" ET ON E.type = ET.id
WHERE lifetime - AGE(usage_start) <= interval '6 months';

-- Вывести список изделий, которых получается больше всего из единицы объема стали
WITH S AS (
  SELECT P.name, quantity
  FROM "Product" P INNER JOIN "ProductMaterials" PM ON P.id = PM.product
  INNER JOIN "Material" M ON PM.material = M.id
  WHERE M.name = 'сталь'
) SELECT name FROM S WHERE quantity IN (SELECT min(quantity) FROM S);

-- Вывести средний срок годности оборудования
SELECT AVG(lifetime)
FROM "Equipment" E INNER JOIN "EquipmentType" ET ON E.type = ET.id;

-- Отсортировать производителей по числу используемого оборудования
SELECT ROW_NUMBER() OVER(ORDER BY COUNT(*) DESC), ET.vendor
FROM "EquipmentType" ET INNER JOIN "Equipment" E ON E.type = ET.id
GROUP BY ET.vendor;

-- Выбрать самую дешёвую замену шёлка
WITH Replacements AS (
  SELECT name, price
  FROM "Material" M INNER JOIN "MaterialAlternatives" MA
  ON M.id = MA.alternative
  WHERE MA.material = 8 -- шёлк
) SELECT name FROM Replacements
WHERE price IN (SELECT min(price) FROM Replacements);

-- Найти цельнометаллические изделия
SELECT name
FROM "Product"
EXCEPT
SELECT P.name
FROM "Product" P INNER JOIN "ProductMaterials" PM ON P.id = PM.product
INNER JOIN "Material" M ON PM.material = M.id
WHERE M.type != 'металл';

-- Вывести стоимость изделий
SELECT P.name, P.id, SUM(quantity * price)
FROM "Product" P INNER JOIN "ProductMaterials" PM ON P.id = PM.product
INNER JOIN "Material" M ON PM.material = M.id
GROUP BY P.name, P.id;
