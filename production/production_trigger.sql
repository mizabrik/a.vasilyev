CREATE OR REPLACE FUNCTION replace_material() RETURNS trigger AS $$
DECLARE
  alternative "Material".id%TYPE;
BEGIN
  SELECT id INTO alternative
  FROM "Material" M INNER JOIN "MaterialAlternatives" MA ON M.id = MA.alternative
  WHERE MA.material = OLD.id ORDER BY price LIMIT 1;

  IF alternative IS NULL THEN
    DELETE FROM "Product" WHERE id IN (
      SELECT product FROM "ProductMaterials" WHERE material = OLD.id
    );
  ELSE
    UPDATE "ProductMaterials" PMO
    SET quantity = quantity + (
      SELECT quantity FROM "ProductMaterials" PM
      WHERE PM.product = PMO.product AND material = OLD.id
    ) WHERE OLD.id IN (SELECT material FROM "ProductMaterials" PM WHERE PM.product = PMO.product)
    AND material = alternative;

    UPDATE "ProductMaterials" PMO
    SET material = alternative
    WHERE material = OLD.id
    AND alternative NOT IN (SELECT material FROM "ProductMaterials" PM WHERE PM.product = PMO.product);

    DELETE FROM "ProductMaterials" WHERE material = OLD.id;
  END IF;
  
  RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER replace_material_on_delete BEFORE DELETE ON "Material"
  FOR EACH ROW EXECUTE PROCEDURE replace_material();
