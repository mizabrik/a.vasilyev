-- 1
SELECT model, speed, hd FROM PC WHERE price < 500;

-- 2
SELECT DISTINCT maker FROM Product WHERE type = 'Printer';

-- 3
SELECT model, RAM, screen FROM Laptop WHERE price > 1000;

-- 4
SELECT * FROM Printer WHERE color = 'y';SELECT model, RAM, screen FROM Laptop WHERE price > 1000;

-- 5
SELECT model, speed, hd FROM PC WHERE price < 600 AND cd IN ('12x', '24x');

-- 6
SELECT DISTINCT Product.maker, Laptop.speed FROM Product INNER JOIN Laptop ON Product.model = Laptop.model WHERE Laptop.hd >= 10;

-- 7
SELECT Product.model, PC.price FROM Product INNER JOIN PC ON PC.Model = Product.model WHERE Product.maker = 'B'
UNION SELECT Product.model, Laptop.price FROM Product INNER JOIN Laptop ON Laptop.Model = Product.model WHERE Product.maker = 'B'
UNION SELECT Product.model, Printer.price FROM Product INNER JOIN Printer ON Printer.Model = Product.model WHERE Product.maker = 'B';

-- 8
SELECT DISTINCT Product.maker FROM Product WHERE type = 'PC' EXCEPT SELECT DISTINCT Product.maker FROM Product WHERE type = 'Laptop';

-- 9
SELECT DISTINCT maker FROM Product INNER JOIN PC ON Product.model = PC.model WHERE PC.speed >= 450;

-- 10
SELECT model, price FROM Printer WHERE price = (SELECT max(price) FROM Printer);

-- 11
SELECT avg(speed) FROM PC;

-- 12
SELECT avg(speed) FROM Laptop WHERE price > 1000;

-- 13
SELECT avg(PC.speed) FROM PC INNER JOIN Product ON PC.model = Product.model WHERE Product.maker = 'A';

-- 14
SELECT DISTINCT maker, max(type) FROM Product GROUP BY maker HAVING min(type) = max(type) AND count(*) > 1;
SELECT DISTINCT maker, type FROM Product WHERE maker IN (SELECT maker FROM PRODUCT GROUP BY maker HAVING count(DISTINCT type) = 1 AND count(model) > 1);

-- 15
SELECT hd FROM PC GROUP BY hd HAVING count(*) > 1;
