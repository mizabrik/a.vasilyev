-- 28
SELECT CAST(avg(qty) AS NUMERIC(10, 2)) FROM (
  SELECT CAST(COALESCE(SUM(B_VOL), 0) as NUMERIC(10, 2)) as qty
  FROM utB RIGHT OUTER JOIN utQ ON B_Q_ID = Q_ID
  GROUP BY Q_ID
) qtys

-- 29
SELECT
  CASE WHEN Inc.point IS NULL THEN Outc.point ELSE Inc.point END point,
  CASE WHEN Inc.date IS NULL THEN Outc.date ELSE Inc.date END date,
  inc,
  out
FROM Income_o AS Inc FULL OUTER JOIN Outcome_o AS Outc
ON Inc.point = Outc.point AND Inc.date = Outc.date;

SELECT
  COALESCE(Inc.point, Outc.point) AS point,
  COALESCE(Inc.date, Outc.date) AS date,
  inc,
  out
FROM Income_o AS Inc FULL OUTER JOIN Outcome_o AS Outc
ON Inc.point = Outc.point AND Inc.date = Outc.date;

-- 30
SELECT point, date,
CASE WHEN sum(out) = 0 THEN NULL ELSE sum(out) END inc,
CASE WHEN sum(inc) = 0 THEN NULL ELSE sum(inc) END inc
FROM (
  SELECT point, date, out, 0 as inc FROM Outcome
  UNION ALL
  SELECT point, date, 0 as out, inc FROM Income
) AS tmp
GROUP BY point, date

-- 31
SELECT class, country FROM Classes WHERE bore >= 16;

-- 32
SELECT country, CAST(avg(POWER(bore, 3) / 2) AS NUMERIC(10,2))
FROM Classes INNER JOIN (
  SELECT class FROM Ships
  UNION ALL
  SELECT DISTINCT ship as class FROM Outcomes WHERE ship NOT IN (
    SELECT name FROM Ships
  )
) AS S ON Classes.class = S.class GROUP BY country

-- 33
SELECT ship FROM Outcomes WHERE battle = 'North Atlantic' AND result = 'sunk';

-- 34
SELECT name FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
WHERE type = 'bb' AND launched >= 1922 AND displacement > 35000;

-- 35
SELECT model, type FROM Product
WHERE model NOT LIKE '%[^a-zA-Z]%' OR model NOT LIKE '%[^0-9]%';

-- 36
SELECT class FROM Classes WHERE class IN (
  SELECT name FROM Ships UNION SELECT ship FROM Outcomes
)

-- 37
SELECT class FROM Classes INNER JOIN (
  SELECT class AS c FROM Ships
  UNION ALL
  SELECT DISTINCT ship AS c FROM Outcomes WHERE ship NOT IN (
    SELECT name FROM Ships
  )
) AS S ON class = S.c GROUP BY class HAVING  count(c) = 1

-- 38
SELECT country FROM Classes GROUP BY country HAVING count(DISTINCT type) > 1;

-- 39
SELECT DISTINCT ship
FROM Outcomes AS O1 INNER JOIN Battles AS B1 ON O1.battle = B1.name
WHERE result = 'damaged' AND ship IN (
  SELECT ship
  FROM Outcomes AS O2 INNER JOIN Battles AS B2 ON O2.battle = B2.name
  WHERE O2.battle != O1.battle AND B2.date > B1.date
)

-- 40
SELECT Ships.class, name, country FROM
Ships INNER JOIN Classes ON Ships.class = Classes.class WHERE numGuns >= 10;

-- 41
SELECT unpv.arg, unpv.value FROM (
  SELECT model,
  CAST(speed as VARCHAR(50)) speed,
  CAST(ram as VARCHAR(50)) ram,
  CAST(hd as VARCHAR(50)) hd,
  CAST(cd as VARCHAR(50)) cd,
  CAST(price as VARCHAR(50)) price
  FROM PC WHERE code IN (SELECT max(code) FROM PC)
) row
CROSS APPLY (
  VAlUES 
  ('model', model),
  ('speed', speed),
  ('ram', ram),
  ('hd', hd),
  ('cd', cd),
  ('price', price)
) AS unpv (arg, value);


-- 42
SELECT ship, battle FROM Battles INNER JOIN Outcomes ON name = battle
WHERE result = 'sunk';

-- 43
SELECT name FROM Battles WHERE YEAR(date) NOT IN (
  SELECT launched FROM Ships WHERE launched IS NOT NULL
);

-- 44
SELECT name FROM Ships WHERE name LIKE 'R%'
UNION
SELECT ship AS name FROM Outcomes WHERE ship LIKE 'R%'

-- 45
SELECT name FROM Ships WHERE name LIKE '% % %'
UNION
SELECT ship AS name FROM Outcomes WHERE ship LIKE '% % %'
