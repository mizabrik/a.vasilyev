-- 46
SELECT ship, displacement, numGuns
FROM Outcomes LEFT OUTER JOIN Ships ON Ships.name = Outcomes.ship
LEFT OUTER JOIN Classes ON Ships.class = Classes.class OR
Outcomes.ship = Classes.class
WHERE battle = 'Guadalcanal'

-- 47
WITH MC(maker, cnt) AS (
  SELECT maker, COUNT(model) FROM Product GROUP BY maker
) SELECT ROW_NUMBER() OVER(ORDER BY cnt DESC, Product.maker, model),
Product.maker, model
FROM Product INNER JOIN MC ON Product.maker = MC.maker;

-- 48
SELECT DISTINCT Classes.class
FROM Classes LEFT OUTER JOIN Ships ON Classes.class = Ships.class
INNER JOIN Outcomes ON name = ship OR Classes.class = ship
WHERE result = 'sunk';

-- 49
SELECT name FROM (
  SELECT name, bore FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
  UNION
  SELECT ship AS name, bore FROM Outcomes INNER JOIN Classes ON ship = class
) AS S WHERE bore = 16

-- 50
SELECT DISTINCT Battles.name
FROM Battles INNER JOIN Outcomes ON battle = name
LEFT OUTER JOIN Ships ON Outcomes.ship = Ships.name 
WHERE class = 'Kongo' OR ship = 'Kongo'

-- 51
WITH S AS (
  SELECT name, displacement, numGuns
  FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
  UNION
  SELECT ship AS name, displacement, numGuns
  FROM Outcomes INNER JOIN Classes ON ship = class
) SELECT name FROM S WHERE numGuns >= ALL (
  SELECT numGuns FROM S AS S2 WHERE S.displacement = S2.displacement
)

-- 52
SELECT name
FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
WHERE type = 'bb' AND country='Japan'
AND (numGuns >= 9 OR numGuns IS NULL)
AND (bore < 19 OR bore IS NULL)
AND (displacement <= 65000 OR displacement IS NULL);


-- 53
SELECT CAST(AVG(CAST(numGuns AS NUMERIC(10,2))) AS NUMERIC(10,2))
FROM classes WHERE type='bb';

-- 54
WITH S AS (
  SELECT name, type, numGuns
  FROM Ships INNER JOIN Classes ON Ships.class = Classes.class
  UNION
  SELECT ship AS name, type, numGuns
  FROM Outcomes INNER JOIN Classes ON ship = class
) SELECT CAST(AVG(CAST(numGuns AS NUMERIC(10,2))) AS NUMERIC(10,2))
FROM S WHERE type = 'bb';

-- 55
SELECT Classes.class, MIN(launched)
FROM Classes LEFT OUTER JOIN Ships ON Classes.class = Ships.class
GROUP BY Classes.class;

-- 56
SELECT class, sum(cnt)
FROM (
  SELECT class, COUNT(*) cnt
  FROM Classes C INNER JOIN Ships S ON C.class = S.class
  INNER JOIN Outcomes ON ship = name AND result = 'sunk'
  GROUP BY C.class
  UNION ALL
  SELECT class, 1 cnt FROM Classes C INNER JOIN Outcomes O ON C.class = O.ship
  WHERE result = 'sunk' AND class NOT IN (SELECT name FROM ships)
  UNION ALL
  SELECT class, 0 cnt FROM Classes
) GROUP BY class

-- 57
SELECT class, sum(cnt)
FROM (
  SELECT C.class, COUNT(*) cnt
  FROM Classes C INNER JOIN Ships S ON C.class = S.class
  INNER JOIN Outcomes ON ship = name AND result = 'sunk'
  GROUP BY C.class
  UNION ALL
  SELECT class, 1 cnt FROM Classes C INNER JOIN Outcomes O ON C.class = O.ship
  WHERE result = 'sunk' AND class NOT IN (SELECT name FROM ships)
  UNION ALL
  SELECT class, 0 cnt FROM Classes
) S WHERE class IN (
  SELECT C1.class FROM Classes C1 INNER JOIN (
    SELECT name, class FROM Ships
    UNION
    SELECT ship name, ship class FROM Outcomes
  ) S1 ON C1.class = S1.class GROUP BY C1.class HAVING COUNT(name) >= 3
) GROUP BY class HAVING sum(cnt) > 0;

-- 58
SELECT M.maker, T.type, CAST(100 * CAST(COUNT(model) AS real) / cnt AS DECIMAL(10,2))
FROM (SELECT DISTINCT maker FROM Product) M
CROSS JOIN (SELECT DISTINCT type FROM Product) T
LEFT OUTER JOIN Product P ON M.maker = P.maker AND T.type = P.type
INNER JOIN (
  SELECT maker, CAST(COUNT(*) AS real) cnt FROM Product GROUP BY maker
) PC ON M.maker = PC.maker GROUP BY M.maker, T.type, cnt

-- 59
SELECT point, SUM(payment) FROM (
  SELECT point, inc as payment FROM Income_o
  UNION ALL
  SELECT point, -out as payment FROM Outcome_o
) AS payments GROUP BY point;

-- 60
SELECT point, SUM(payment) FROM (
  SELECT point, inc as payment FROM Income_o WHERE date < '2001-04-15'
  UNION ALL
  SELECT point, -out as payment FROM Outcome_o WHERE date < '2001-04-15'
) AS payments GROUP BY point;

