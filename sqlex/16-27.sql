-- 16
SELECT DISTINCT PC.model, PC2.model, PC.speed, PC.ram
FROM PC INNER JOIN PC AS PC2
ON PC.speed = PC2.speed AND PC.ram = PC2.ram
WHERE PC.model > PC2.model;

-- 17
SELECT DISTINCT Product.type, Product.model, speed
FROM Product INNER JOIN Laptop
ON Product.model = Laptop.model
WHERE speed < ALL (SELECT speed FROM PC);

-- 18
SELECT maker, min(price)
FROM Product INNER JOIN Printer 
ON Product.model = Printer.model
GROUP BY maker
HAVING min(price) = (SELECT min(price) FROM Printer WHERE color = 'y');

-- 19
SELECT maker, avg(screen)
FROM Product INNER JOIN Laptop
ON Product.model = Laptop.model
GROUP BY maker;

-- 20
SELECT maker, count(model)
FROM Product
WHERE type = 'PC'
GROUP BY maker
HAVING count(model) >= 3;

-- 21
SELECT maker, max(PC.price)
FROM Product INNER JOIN PC
ON Product.model = PC.model
GROUP BY maker;

-- 22
SELECT speed, avg(price)
FROM PC
WHERE speed > 600
GROUP BY speed;

-- 23
SELECT DISTINCT maker
FROM Product
WHERE maker in (
  SELECT maker
  FROM Product INNER JOIN PC
  ON Product.model = PC.model
  WHERE speed >= 750
  INTERSECT
  SELECT maker
  FROM Product INNER JOIN Laptop
  ON Product.model = Laptop.model
  WHERE speed >= 750
);

-- 24
WITH Prices AS (
  SELECT Product.model, price FROM Product INNER JOIN PC ON Product.model = PC.model
  UNION
  SELECT Product.model, price FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
  UNION
  SELECT Product.model, price FROM Product INNER JOIN Printer ON Product.model = Printer.model
) SELECT DISTINCT model
FROM Prices
WHERE price = (SELECT max(price) FROM Prices);

-- 25
SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer' AND maker IN (
  SELECT maker
  FROM Product INNER JOIN PC
  ON Product.model = PC.model
  WHERE ram IN (
    SELECT min(ram)
    FROM PC
    ) AND speed IN (
    SELECT max(speed)
    FROM PC
    WHERE ram IN (
      SELECT min(ram)
      FROM PC
)));

SELECT DISTINCT PrinterMaker.maker
FROM (
  SELECT maker
  FROM Product INNER JOIN PC
  ON Product.model = PC.model
  INNER JOIN (
    SELECT max(speed) max_speed, min(ram) min_ram
    FROM PC
    WHERE ram in (SELECT min(ram) FROM PC)) AS Stats
  ON PC.speed = Stats.max_speed AND PC.ram = Stats.min_ram
) AS PCMaker INNER JOIN (
  SELECT maker FROM Product WHERE type = 'Printer'
) AS PrinterMaker
ON PCMaker.maker = PrinterMaker.maker;

-- 26
SELECT avg(price)
FROM Product INNER JOIN (
  SELECT model, price
  FROM PC
  UNION ALL
  SELECT model, price
  FROM Laptop
) Price ON Product.model = Price.model
WHERE maker = 'A';

-- 27
SELECT maker, avg(hd)
FROM Product INNER JOIN PC
ON Product.model = PC.model
GROUP BY maker
HAVING maker IN (
  SELECT maker
  FROM Product
  WHERE type = 'Printer'
);
